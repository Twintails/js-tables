//delete the row then get row count and iterate forward to update id's, if there are no rows, there wont be a delete button needed. 
function deleteRow(rowID) 
{
	var tbl=document.getElementById("table_main");
		tbl.deleteRow(rowID);
	var trs=tbl.getElementsByTagName("tr");
	
	var trsl=trs.length;
	
	for (var i=rowID; i<trsl; i++)
	{	
			trs[i].setAttribute("id", "R"+i);
		var tds = trs[i].getElementsByTagName("td");
		var tdsl= tds.length;
		var tdata=tds[0];
			tdata.setAttribute('id', "R"+i+"c1");
			tdata.setAttribute('name', "R"+i+"c1");
		var tdzerolinks = tdata.getElementsByTagName('button');
		var tdzeroazero = tdzerolinks[0];
		var tdzeroaone = tdzerolinks[1];
			tdzeroazero.setAttribute('onclick', "deleteRow('"+i+"')");
			tdzeroazero.textContent=("- Row "+i);
			tdzeroaone.setAttribute('onclick', "addRowBelow('"+i+"')");
			tdzeroaone.textContent=("+ After "+i);
		for (var e=1; e<tds.length; e++)
		{	
			var rice=("R"+i+"c"+e);
				tdata=tds[e];
				tdata.setAttribute('ondblclick', "toggleRowEditable('R"+i+"', false)");
				tdata.setAttribute('id', rice);
				tdata.setAttribute('name', rice);
		}
	}
	return false;
}

// create new row, get row count and iterate back to change row id's following the new one...
// also, if there are no rows, we need to add the row to the tablebody instead of the table header...
function addRowBelow(rowID) 
{
	var tbl=document.getElementById("table_main");
	var tbody=document.getElementById('tbody-main');
	var n=parseInt(rowID, 0)+1;
	var trs=tbl.getElementsByTagName("tr");
	var trsl=trs.length;
	if (trsl==1) //if there is only one, it's probably the header row withe the column names. 
	{
		var tds=trs[0].getElementsByTagName('th'); // this would probably break if there is a colspan on the header. We can cross that bridge if we come to it. 
		var newRow=tbody.insertRow(n-1); //add row to tbody if there are none
	}
	else	
	{
		var tds=trs[1].getElementsByTagName('td');
		var newRow=tbl.insertRow(n); //there is already a table, so lets add new rows to it
	}
	var tdsl=tds.length;

		newRow.setAttribute("id", "R"+n);
	for (var e=0; e<tdsl; e++)
	{
		var tdata=newRow.insertCell(e);
		var rice=("R"+n+"c"+e);
			tdata.setAttribute('id', rice);
			tdata.setAttribute('name', rice);
		if (e==0)
		{
			tdata.setAttribute("contenteditable", "false");
			tdatalzero=document.createElement('button');
			tdatalzero.setAttribute('onclick', "deleteRow('"+n+"')");
			tdatalzero.appendChild(document.createTextNode("- Row "+n));
			tdata.appendChild(tdatalzero);
			tdata.appendChild(document.createElement("br"));
			tdatalone=document.createElement('button');
			tdatalone.setAttribute('onclick', "addRowBelow('"+n+"')");
			tdatalone.appendChild(document.createTextNode("+ After "+n));
			tdata.appendChild(tdatalone);
		}
		else if (e!=5)
		{
			tdata.setAttribute('ondblclick', "toggleRowEditable('R"+n+"', false)");
			tdata.setAttribute("contenteditable", "inherit");
			tdata.appendChild(document.createTextNode("N "+n+', '+e));
		}
		else 
		{
			tdata.setAttribute("contenteditable", "false");
			tdatal=document.createElement('a');
			tdatal.setAttribute("href", "javascript:void(0)");
			tdatal.appendChild(document.createTextNode("N "+n+', '+e));
			tdata.appendChild(tdatal);
		}
	}

		
	for (var i=trsl; i>(n); i--)
	{	
			trs[i].setAttribute("id", "R"+i);
		var tds=trs[i].getElementsByTagName('td');
		var tdata=tds[0];
			tdata.setAttribute('id', "R"+i+"c1");
			tdata.setAttribute('name', "R"+i+"c1");
		var tdzerolinks = tdata.getElementsByTagName("button");
		var tdzeroazero = tdzerolinks[0];
		var tdzeroaone = tdzerolinks[1];
			tdzeroazero.setAttribute('onclick', "deleteRow('"+i+"')");
			tdzeroazero.textContent=("- Row "+i);
			tdzeroaone.setAttribute('onclick', "addRowBelow('"+i+"')");
			tdzeroaone.textContent=("+ After "+i);
		for (var e=1; e<tds.length; e++)
		{	
			var rice=("R"+i+"c"+e);
				tdata=tds[e];
				tdata.setAttribute('ondblclick', "toggleRowEditable('R"+i+"', false)");
				tdata.setAttribute('id', rice);
				tdata.setAttribute('name', rice);
		}
	}
	return false;
}

//	Make sure we are a real boy
function realBoy() {
	if (!document.getElementById) {
		alert("This page doesn't seem to be a webpage...");
		return;
	}
	else {
		bodycontent=document.getElementById('bodycontent');
		bodycontent.innerHTML='';

		instructions=document.createElement('div');
		instructions.setAttribute('id', 'instructions');
		instructions.setAttribute('class', 'container');
		instructions.appendChild(document.createTextNode("Double-click on a row to edit. Double-click again to lock edits."));
		bodycontent.appendChild(instructions);
		
		container=document.createElement('div');
		container.setAttribute('id', 'tablecontainer');
		container.setAttribute('class', 'container');
		bodycontent.appendChild(container);
		
		makeTable();
		return;
	}
}

//  addEvent function by Scott Andrews 
function addEvent(elm, evType, fn, useCapture) 
{
	if (elm.addEventListener) 
	{
		elm.addEventListener(evType, fn, useCapture);
		return true;
	} 
	else if (elm.attachEvent) 
	{
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	} 
	else 
	{
		elm['on' + evType] = fn;
	}
}


// Call a toggle in the original table buld; add toggleRowEditable function
// Call a toggle ondblclick events in the cells
// Exclude first cell from toggling for add/delete links
// Exclude last cell from toggling flor external links
 
function toggleRowEditable(rowID, condition) {
	var trow=document.getElementById(rowID);
	var tds = trow.getElementsByTagName("td");
	var tdsl = tds.length;
	
	if (trow!=null) 
	{
		switch(condition)
		{
		    case true:
				condition=false;
				break;
		    case false:
				condition=true;
				break;
	    }
	    
	    for (var y=1; y<(tdsl-1);y++) //exclude first cell and last cell from toggling
	    {	
	    	tds[y].setAttribute("ondblclick", "toggleRowEditable('"+rowID+"', "+condition+")");
	    	tds[y].setAttribute("contenteditable", condition);
	    }  
	}
	return ;		
}


//	Feed the realBoy, poor thing needs content
//  This makes the table and calls toggle to set the ondblclick listener
//  Checks for column number along the way
//	first and last Column recieve no toggleRowEditable fn

function makeTable(){
	var c=5;
	var r=10;
	var trow;
	var tdata;
	var tdatal;
	var container=document.getElementById('tablecontainer');
	var table=document.createElement('table');
		table.setAttribute("id", "table_main");
		table.setAttribute("contenteditable", "false");
	var thead=document.createElement('thead');
	var theadrow=document.createElement('tr');
		theadrow.setAttribute("id", "header_row");
	for(var j=1;j<=(c+1);j++)
	{
		
		tdata=document.createElement('th');
		if(j==1)
		{
			tdatalone=document.createElement('button');
			tdatalone.setAttribute('onclick', "addRowBelow('0')");
			tdatalone.appendChild(document.createTextNode("Add Row"));
			tdata.appendChild(tdatalone);
		}
		else 
		{
			tdata.appendChild(document.createTextNode('Column '+(j-1)));
		};
		
		theadrow.appendChild(tdata);
	}
		thead.appendChild(theadrow);	
		table.appendChild(thead);
	var tfoot=document.createElement('tfoot');
		table.appendChild(tfoot);
	var tbody=document.createElement('tbody');
		tbody.setAttribute('id', 'tbody-main');
		
	for(var i=1;i<=r;i++)
	{
		var ri=("R"+i);
		trow=document.createElement('tr');
		trow.setAttribute("id", ri);
		for(var e=1;e<=(c+1);e++)
		{
			var rice=("R"+i+"c"+e);
			tdata=document.createElement('td');
			tdata.setAttribute("id", rice);
			tdata.setAttribute("name", rice);
			if (e==1)
			{
				tdata.setAttribute("contenteditable", "false");
				tdatalzero=document.createElement('button');
				tdatalzero.setAttribute('onclick', "deleteRow('"+i+"')");
				tdatalzero.appendChild(document.createTextNode("- Row "+i));
				tdata.appendChild(tdatalzero);
				tdata.appendChild(document.createElement("br"));
				tdatalone=document.createElement('button');
				tdatalone.setAttribute('onclick', "addRowBelow('"+i+"')");
				tdatalone.appendChild(document.createTextNode("+ After "+i));
				tdata.appendChild(tdatalone);
			}
			else if (e!=6)
			{
				tdata.setAttribute('ondblclick', "toggleRowEditable('"+ri+"', false)");
				tdata.setAttribute("contenteditable", "inherit");
				tdata.appendChild(document.createTextNode(i+', '+(e-1)));
			}
			else 
			{
				tdata.setAttribute("contenteditable", "false");
				tdatal=document.createElement('a');
				tdatal.setAttribute("href", "javascript:void(0)");
				tdatal.appendChild(document.createTextNode(i+', '+(e-1)));
				tdata.appendChild(tdatal);
			}
			trow.appendChild(tdata);
		}
		tbody.appendChild(trow);
		
	}
	table.appendChild(tbody);
	container.appendChild(table);
	return false;
}