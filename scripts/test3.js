//make sure we are a real boy
function realBoy() {
	if (!document.getElementById) {
		alert("This page doesn't seem to be a webpage...");
		return;
	}
	else {
		bodycontent=document.getElementById('bodycontent');
		bodycontent.innerHTML='';

		instructions=document.createElement('div');
		instructions.setAttribute('id', 'instructions');
		instructions.setAttribute('class', 'container');
		instructions.appendChild(document.createTextNode("Double-click on a row to edit. Double-click again to lock edits."));
		bodycontent.appendChild(instructions);
		
		container=document.createElement('div');
		container.setAttribute('id', 'tablecontainer');
		container.setAttribute('class', 'container');
		bodycontent.appendChild(container);
		
		makeTable();
		return;
	}
}

// Call a toggle in the original table buld; add toggleRowEditable function
// Call a toggle ondblclick events in the cells
// Exclude last cell from toggling for links
 
function toggleRowEditable(rowID, condition) {
	var trow=document.getElementById(rowID)
	var tds = trow.getElementsByTagName("td");
	var tdsl = tds.length;
	
	if (trow!=null) 
	{
		switch(condition)
		{
		    case true:
				condition=false;
				break;
		    case false:
				condition=true;
				break;
	    }
	    
	    for (var y=0; y<(tdsl-1);y++) //exclude first cell and last cell from toggling
	    {	
	    	tds[y].setAttribute("ondblclick", "toggleRowEditable('"+rowID+"', "+condition+")");
	    	tds[y].setAttribute("contenteditable", ""+condition+"");
	    }  
	}
	return ;		
}


//  addEvent function by Scott Andrews 
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
	elm.addEventListener(evType, fn, useCapture);
	return true;
	} else if (elm.attachEvent) {
	var r = elm.attachEvent('on' + evType, fn);
	return r;
	} else {
	elm['on' + evType] = fn;
	}
}


//	Feed the realBoy, poor thing needs content
//  This makes the table and calls toggle to set the ondblclick listener
//  Checks for column number along the way
//	4th Column gets no toggleRowEditable fn

function makeTable(){
var c=4;
var r=4;
var trow;
var tdata;
var tdatal;
var container=document.getElementById('tablecontainer');
var table=document.createElement('table');
	table.setAttribute("id", "table_main");
	table.setAttribute("contenteditable", "false")
var thead=document.createElement('thead');
var theadrow=document.createElement('tr');
	theadrow.setAttribute("id", "header_row");
for(var j=1;j<=(c+1);j++){
	tdata=document.createElement('th');
	tdata.appendChild(document.createTextNode('Column '+j))
	theadrow.appendChild(tdata);
	};
thead.appendChild(theadrow);	
table.appendChild(thead);
var tfoot=document.createElement('tfoot');
table.appendChild(tfoot);
var tbody=document.createElement('tbody');

for(var i=1;i<=(r+1);i++){
	var ri=("R"+i);
	trow=document.createElement('tr');
	trow.setAttribute("id", ri);
	for(var e=1;e<=(c+1);e++){
		var rice=("R"+i+"c"+e);
		tdata=document.createElement('td');
		tdata.setAttribute("id", rice);
		tdata.setAttribute("name", rice);
		if (e!=5){
			tdata.setAttribute('ondblclick', "toggleRowEditable('"+ri+"', false)");
			tdata.setAttribute("contenteditable", "inherit");
			tdata.appendChild(document.createTextNode(i+', '+e));
		}
		else {
			tdata.setAttribute("contenteditable", "false");
			tdatal=document.createElement('a');
			tdatal.setAttribute("href", "#");
			tdatal.appendChild(document.createTextNode(i+', '+e));
			tdata.appendChild(tdatal);
		}
		trow.appendChild(tdata);
	}
	tbody.appendChild(trow);
	
};
table.appendChild(tbody);
container.appendChild(table);
}