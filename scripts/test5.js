var newCurrentPage;
var r=113;
var limit=10;
var pagesshown=5;
var pagers;
//delete the row then get row count and iterate forward to update id's, if there are no rows, there wont be a delete button needed. 
function deleteRow(rowID) 
{
	var tbl=document.getElementById("maintable");
		tbl.deleteRow(rowID);
	var trs=tbl.getElementsByTagName("tr");
	
	var trsl=trs.length;
	
	for (var i=rowID; i<trsl; i++)
	{	
			trs[i].setAttribute("id", "R"+i);
		var tds = trs[i].getElementsByTagName("td");
		var tdsl= tds.length;
		var tdata=tds[0];
			tdata.setAttribute('id', "R"+i+"c1");
			tdata.setAttribute('name', "R"+i+"c1");
		var tdzerolinks = tdata.getElementsByTagName('button');
		var tdzeroazero = tdzerolinks[0];
		var tdzeroaone = tdzerolinks[1];
			tdzeroazero.setAttribute('onclick', "deleteRow('"+i+"')");
			tdzeroazero.textContent=("- Row "+i);
			tdzeroaone.setAttribute('onclick', "addRowBelow('"+i+"')");
			tdzeroaone.textContent=("+ After "+i);
		for (var e=1; e<tds.length; e++)
		{	
			var rice=("R"+i+"c"+e);
				tdata=tds[e];
				tdata.setAttribute('ondblclick', "toggleRowEditable('R"+i+"', false)");
				tdata.setAttribute('id', rice);
				tdata.setAttribute('name', rice);
		}
	}
	
	//I believe, to get the nav to reload as  rows are removed, we aught to call it again and again. Maybe there would be a better way to handle this?
	
	addEvent('maintbody', 'change',	newPager('pagenav', newCurrentPage),	false);
	
	return false;
}



// create new row, get row count and iterate back to change row id's following the new one...
// also, if there are no rows, we need to add the row to the tablebody instead of the table header...
// Known "bug" header "Add Row" button only adds rows to the top of the table. 
// With further work header "Add Row" button might disappear if the table has rows, 
//	Header "Add Row" might be set to add rows to the top of the "page" the client browser is currently viewing 

function addRowBelow(rowID) 
{
	r++;
	var tbl=document.getElementById("maintable");
	var tbody=document.getElementById('maintbody');
	var n=parseInt(rowID, 0)+1;
	var trs=tbl.getElementsByTagName("tr");
	var trsl=trs.length;
	
	if (trsl==1) //if there is only one, it's probably the header row withe the column names. 
	{
		var tds=trs[0].getElementsByTagName('th'); // this would probably break if there is a colspan on the header. We can cross that bridge if we come to it. 
		var newRow=tbody.insertRow(n-1); //add row to tbody if there are none
	}
	else	
	{
		var tds=trs[1].getElementsByTagName('td');
		var newRow=tbl.insertRow(n); //there is already a table, so lets add new rows to it
	}
	var tdsl=tds.length;

		newRow.setAttribute("id", "R"+n);
	for (var e=0; e<tdsl; e++)
	{
		var tdata=newRow.insertCell(e);
		var rice=("R"+n+"c"+e);
			tdata.setAttribute('id', rice);
			tdata.setAttribute('name', rice);
		if (e==0)
		{
			tdata.setAttribute("contenteditable", "false");
			tdatalzero=document.createElement('button');
			tdatalzero.setAttribute('onclick', "deleteRow('"+n+"')");
			tdatalzero.appendChild(document.createTextNode("- Row "+n));
			tdata.appendChild(tdatalzero);
			tdata.appendChild(document.createElement("br"));
			tdatalone=document.createElement('button');
			tdatalone.setAttribute('onclick', "addRowBelow('"+n+"')");
			tdatalone.appendChild(document.createTextNode("+ After "+n));
			tdata.appendChild(tdatalone);
		}
		else if (e!=5)
		{
			tdata.setAttribute('ondblclick', "toggleRowEditable('R"+n+"', false)");
			tdata.setAttribute("contenteditable", "inherit");
			tdata.appendChild(document.createTextNode("N "+n+', '+e));
		}
		else 
		{
			tdata.setAttribute("contenteditable", "false");
			tdatal=document.createElement('a');
			tdatal.setAttribute("href", "javascript:void(0)");
			tdatal.appendChild(document.createTextNode("N "+n+', '+e));
			tdata.appendChild(tdatal);
		}
	}

		
	for (var i=trsl; i>(n); i--)
	{	
			trs[i].setAttribute("id", "R"+i);
		var tds=trs[i].getElementsByTagName('td');
		var tdata=tds[0];
			tdata.setAttribute('id', "R"+i+"c1");
			tdata.setAttribute('name', "R"+i+"c1");
		var tdzerolinks = tdata.getElementsByTagName("button");
		var tdzeroazero = tdzerolinks[0];
		var tdzeroaone = tdzerolinks[1];
			tdzeroazero.setAttribute('onclick', "deleteRow('"+i+"')");
			tdzeroazero.textContent=("- Row "+i);
			tdzeroaone.setAttribute('onclick', "addRowBelow('"+i+"')");
			tdzeroaone.textContent=("+ After "+i);
		for (var e=1; e<tds.length; e++)
		{	
			var rice=("R"+i+"c"+e);
				tdata=tds[e];
				tdata.setAttribute('ondblclick', "toggleRowEditable('R"+i+"', false)");
				tdata.setAttribute('id', rice);
				tdata.setAttribute('name', rice);
		}
	}
	
	//I believe, to get the nav to reload as new rows are generated, we aught to call it again and again. Maybe there would be a better way to handle this?

	
	addEvent('maintbody', 'change',	newPager('pagenav', newCurrentPage),	false);
	
	
	return false;
}

//	Make sure we are a real boy
function realBoy() {
	if (!document.getElementById) {
		alert("This page doesn't seem to be a webpage...");
		return;
	}
	else {
		bodycontent=document.getElementById('bodycontent');
		bodycontent.innerHTML='';

		instructions=document.createElement('div');
		instructions.setAttribute('id', 'instructions');
		instructions.setAttribute('class', 'container');
		br=document.createElement('br');
		instructions.appendChild(document.createTextNode("Double-click on a row to edit. Double-click again to lock edits."));
		instructions.appendChild(br);
		instructions.appendChild(document.createTextNode("While the double click to make editable is nifty, it's cumbersome to deal with on a touch device. As well, if you want to double click the text to select it, doing so locks the row: it's past time I setup an Edit/Save button."));
		bodycontent.appendChild(instructions);
		
		container=document.createElement('div');
		container.setAttribute('id', 'tablecontainer');
		container.setAttribute('class', 'container');
		bodycontent.appendChild(container);
		
		makeTable();
		return;
	}
}

//  addEvent function by Scott Andrews 
function addEvent(elm, evType, fn, useCapture) 
{	
	if (elm.addEventListener) 
	{
		elm.addEventListener(evType, fn, useCapture);
		return true;
	} 
	else if (elm.attachEvent) 
	{
		var ret = elm.attachEvent('on' + evType, fn);
		return ret;
	} 
	else 
	{
		elm['on' + evType] = fn;
	}
}


// Call a toggle in the original table buld; add toggleRowEditable function
// Call a toggle ondblclick events in the cells
// Exclude first cell from toggling for add/delete links
// Exclude last cell from toggling flor external links
 
function toggleRowEditable(rowID, condition) {
	var trow=document.getElementById(rowID);
	var tds = trow.getElementsByTagName("td");
	var tdsl = tds.length;
	
	if (trow!=null) 
	{
		switch(condition)
		{
		    case true:
				condition=false;
				break;
		    case false:
				condition=true;
				break;
	    }
	    
	    for (var y=1; y<(tdsl-1);y++) //exclude first cell and last cell from toggling
	    {	
	    	tds[y].setAttribute("ondblclick", "toggleRowEditable('"+rowID+"', "+condition+")");
	    	tds[y].setAttribute("contenteditable", condition);
	    	tds[y].className=("is"+condition);
	    }  
	}
	return ;
}


//Attr: Luigi Vaggiano @ newinstance.it original Pager included in zip archive. 
//neede to have two nav locales (above and below table) so I hacked it a bit to get what I needed

//My paging goal: first jump previous (Page group) next jump last

function Pager(tableName, itemsPerPage, pagerName, positionId, pageNum) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.pagerName = pagerName;
    this.positionId = positionId;
    this.currentPage = pageNum;
    newCurrentPage=pageNum;
    this.pages = 0;
    this.inited = false;
    this.numbers = new Array(pagesshown);


    this.showRecords = function (from, to) {
        var table = document.getElementById(tableName);
        var rows = table.rows;
        
        for (var i = 0; i < rows.length; i++) {
            if (i < from || i > to) rows[i].style.display = 'none';
            else rows[i].style.display = '';
        }
    }


    this.showPage = function (pageNumber) {
    newCurrentPage = pageNumber;
    var elechildren;
    var eleChild;
    
        if (!this.inited) {
            alert("not inited");
            return;
        }


		var pagenavelements= document.getElementsByName(this.positionId);

		
        if (this.isRedrawNeeded(pageNumber)) {
            var startPage = Math.floor((pageNumber - 1) * 0.2) * pagesshown;
            this.showPageNav(startPage + 1);
        }
		
		
		
		
		
		//hack to change the pagers elements from two locations on the page set normal
		
		for (navele=0; navele<pagenavelements.length; navele++) 
		{ 
			elechildren = pagenavelements[navele].childNodes;
			for(var ch = 0; ch < elechildren.length; ch++)
			{
				if(elechildren.item(ch).id == (this.pagerName + 'pg' + this.currentPage))
				{
					eleChild = elechildren.item(ch);
					break;
				}
			}
		    var oldPageAnchor = eleChild;
	        if (oldPageAnchor != null) 
	        {
	            oldPageAnchor.className = 'pg-normal';
	        }
		}
		
		//hack to change the pagers elements from two locations on the page set selected
		for (navele=0; navele<pagenavelements.length; navele++) 
		{ 
			elechildren = pagenavelements[navele].childNodes;		
	
	        this.currentPage = pageNumber;
	        for(var ch = 0; ch < elechildren.length; ch++)
	        {
	        	if(elechildren.item(ch).id == (this.pagerName + 'pg' + this.currentPage))
		        {
		        	eleChild = elechildren.item(ch);
		        	break;
		        }
	        }
	        var newPageAnchor = eleChild;
	        if (newPageAnchor != null) 
	        {
	            newPageAnchor.className = 'pg-selected';
	        }
		}
		
		var from = (pageNumber - 1) * itemsPerPage;
		var to = (pageNumber * itemsPerPage) - 1;
		this.showRecords(from, to);
		
		for (navele=0; navele<pagenavelements.length; navele++) 
		{ 
			elechildren = pagenavelements[navele].childNodes;
		
	       
	
			for(var ch = 0; ch < elechildren.length; ch++)
			{
				if(elechildren.item(ch).id == (this.pagerName + 'pgNext'))
			    {	
	        		var pgNext = elechildren.item(ch);
	        	}
	        	else if  (elechildren.item(ch).id == (this.pagerName + 'pgPrev'))
	        	{
	        		var pgPrev = elechildren.item(ch);
	        	}
	        }
	        
	
	
	        if (pgNext != null) {
	            if (this.currentPage == this.pages) pgNext.style.display = 'none';
	            else pgNext.style.display = '';
	        }
	        if (pgPrev != null) {
	            if (this.currentPage == 1) pgPrev.style.display = 'none';
	            else pgPrev.style.display = '';
	        }
		}
    }


    this.prev = function () {
        if (this.currentPage > 1) this.showPage(this.currentPage - 1);
    }


    this.next = function () {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }


    this.init = function () {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }




    this.isRedrawNeeded = function (pageNumber) {


        for (var i = 0; i < this.numbers.length; i++) {
            if (this.numbers[i] == pageNumber) {
                return false;
            }
        }
        return true;
    }




    this.showPageNav = function (start) {
        if (!this.inited) {
            alert("not inited");
            return;
        }
        
        var pagenavelements= document.getElementsByName(this.positionId);

        var loopEnd = start + (pagesshown-1);
        var index = 0;
        this.numbers = new Array(pagesshown);


        var pagerHtml = '<span class=\"regularData\">' + (document.getElementById(this.tableName).rows.length) + ' Rows: </span>';

		//First &#12298;   =   《
        pagerHtml += '<span onclick="' + this.pagerName + '.showPage(1);" class="pg-normal">&#12298 </span>';


        if (this.pages > 1) {
        	//Prev &#12296;   =   〈
            pagerHtml += '<span id="' + this.pagerName + 'pgPrev" onclick="' + this.pagerName + '.prev();" class="pg-normal"> &#12296 </span>';
        }
        
        if (start > pagesshown) {
            pagerHtml += '<span id="' + this.pagerName + 'pg' + (start-1) + '" class="pg-normal" onclick="' + this.pagerName + '.showPage(' + (start-1) + ');">...</span>';
        }
        
        for (var page = start; page <= loopEnd; page++) {			

            if (page > this.pages) {
                break;
            }
            this.numbers[index] = page;
            pagerHtml += '<span id="' + this.pagerName + 'pg' + page + '" class="pg-normal" onclick="' + this.pagerName + '.showPage(' + page + ');">' + page + '</span>';
            if (page != loopEnd) {
                pagerHtml += '';
            }
            index++;
        }
        page--;
        if (this.pages > page) {
            pagerHtml += '<span id="' + this.pagerName + 'pg' + (page+1) + '" class="pg-normal" onclick="' + this.pagerName + '.showPage(' + (page+1) + ');">...</span>';
        }

		//Next &#12297;   =   〉
        pagerHtml += '<span id="' + this.pagerName + 'pgNext" onclick="' + this.pagerName + '.next();" class="pg-normal"> &#12297 </span>';
        
        //Last &#12299;   =   》
        if (this.pages<1) {this.pages=1};
        pagerHtml += '<span onclick="' + this.pagerName + '.showPage(' + this.pages + ');" class="pg-normal"> &#12299</span>';
	        
		for (navele=0; navele<pagenavelements.length; navele++) 
		{   
		    var element = pagenavelements[navele];
	        element.innerHTML = pagerHtml;
	    }
    }
}

//	Feed the realBoy, poor thing needs content
//  This makes the table and calls toggle to set the ondblclick listener
//  Checks for column number along the way
//	first and last Column recieve no toggleRowEditable fn

function newPager(navId, pageNum) {
	pagers = new Pager('maintbody', limit, 'pagers', navId, pageNum);
 	pagers.init(); 		
 	pagers.showPageNav('pagers');
 	pagers.showPage(pageNum);
 	newCurrentPage=pageNum;
 	return pagers, newCurrentPage;
};


function makeTable(){
	var c=5;
	var container=document.getElementById('tablecontainer');
	var page=1;
	newCurrentPage = page;
	var trow;
	var tdata;
	var tdatal;
	
	//Top Nav
	var pagenavtop=document.createElement('div');
		pagenavtop.setAttribute('id', 'pagenavtop')
		pagenavtop.setAttribute('class', 'pagenav');
		pagenavtop.setAttribute('name', 'pagenav');
	
	//Bottom Nav
	var pagenavbottom=document.createElement('div');
		pagenavbottom.setAttribute('id', 'pagenavbottom');
		pagenavbottom.setAttribute('class', 'pagenav');
		pagenavbottom.setAttribute('name', 'pagenav');	
	
	// Table
	var table=document.createElement('table')
		table.setAttribute('id', 'maintable');
		table.setAttribute("contenteditable", "false");
		
	//Table Header 	
	var thead=document.createElement('thead')
		thead.setAttribute('id', 'thead');
		
	//Table Header Row	
	var theadrow=document.createElement('tr');
		theadrow.setAttribute("id", "header_row");
		for(var j=1;j<=(c+1);j++)
		{
			
			tdata=document.createElement('th');
			if(j==1)
			{
				tdatalone=document.createElement('button');
				tdatalone.setAttribute('onclick', "addRowBelow('0')");
				tdatalone.appendChild(document.createTextNode("Add Row"));
				tdata.appendChild(tdatalone);
			}
			else 
			{
				tdata.appendChild(document.createTextNode('Column '+(j-1)));
			};
			
			theadrow.appendChild(tdata);
		}
		thead.appendChild(theadrow);	
	//End Table Header Row
	
		table.appendChild(thead);
	//End Table Header
		
	//Table Footer
	var tfoot=document.createElement('tfoot');
		tfoot.setAttribute('id', 'tfoot')
		table.appendChild(tfoot);
	//End Table Footer
		
		
	//Table Body
	var tbody=document.createElement('tbody');
		tbody.setAttribute('id', 'maintbody');
		tbody.setAttribute('class', 'tbody')
		
		//Table Body Rows
		for(var i=1;i<=r;i++)
		{
			var ri=("R"+i);
			trow=document.createElement('tr');
			trow.setAttribute("id", ri);
			for(var e=1;e<=(c+1);e++)
			{
				var rice=("R"+i+"c"+e);
				
				//Table Body Row Cells
				tdata=document.createElement('td');
				tdata.setAttribute("id", rice);
				tdata.setAttribute("name", rice);
				if (e==1)
				{
					tdata.setAttribute("contenteditable", "false");
					tdatalzero=document.createElement('button');
					tdatalzero.setAttribute('onclick', "deleteRow('"+i+"')");
					tdatalzero.appendChild(document.createTextNode("- Row "+i));
					tdata.appendChild(tdatalzero);
					tdata.appendChild(document.createElement("br"));
					tdatalone=document.createElement('button');
					tdatalone.setAttribute('onclick', "addRowBelow('"+i+"')");
					tdatalone.appendChild(document.createTextNode("+ After "+i));
					tdata.appendChild(tdatalone);
				}
				else if (e!=6)
				{
					tdata.setAttribute('ondblclick', "toggleRowEditable('"+ri+"', false)");
					tdata.setAttribute("contenteditable", "inherit");
					tdata.appendChild(document.createTextNode(i+', '+(e-1)));
				}
				else 
				{
					tdata.setAttribute("contenteditable", "false");
					tdatal=document.createElement('a');
					tdatal.setAttribute("href", "javascript:void(0)");
					tdatal.appendChild(document.createTextNode(i+', '+(e-1)));
					tdata.appendChild(tdatal);
				}
				trow.appendChild(tdata);
				//End Table Body Row Cells
				
			}
			tbody.appendChild(trow);
			//End Table Body Rows
		}
		table.appendChild(tbody);
		//End Table Body
		
		
		//Append Stuff to Container
		container.appendChild(pagenavtop);
		container.appendChild(table);
		container.appendChild(pagenavbottom);
		//End Append
	
	
	addEvent('maintbody', 'change',	newPager('pagenav', newCurrentPage),	false);
	
	return ;
	
	
}




