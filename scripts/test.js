//make sure we are a real boy
function realBoy() {
	if (!document.getElementById) {
		alert("This page doesn't seem to be a webpage...");
		return;
	}
	else {
		bodycontent=document.getElementById('bodycontent');
		console.log(bodycontent);
		bodycontent.innerHTML='';
		container=document.createElement('div');
		container.setAttribute('id', 'tablecontainer');
		container.setAttribute('class', 'container');
		console.log(container);
		bodycontent.appendChild(container);
		makeTable();
		return;
	}
}

//  addEvent function by Scott Andrews 
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
	elm.addEventListener(evType, fn, useCapture);
	return true;
	} else if (elm.attachEvent) {
	var r = elm.attachEvent('on' + evType, fn);
	return r;
	} else {
	elm['on' + evType] = fn;
	}
}


//	Feed the realBoy... poor thing needs content
function makeTable(){
var c=4;
var r=4;
var trow;
var tdata;
var container=document.getElementById('tablecontainer');
var table=document.createElement('table');
	table.setAttribute("id", "table_main");
var thead=document.createElement('thead');
var theadrow=document.createElement('tr');
	theadrow.setAttribute("id", "header_row");
for(var j=1;j<=(c+1);j++){
	tdata=document.createElement('th');
	tdata.appendChild(document.createTextNode('Column '+j))
	theadrow.appendChild(tdata);
	};
thead.appendChild(theadrow);	
table.appendChild(thead);
var tfoot=document.createElement('tfoot');
table.appendChild(tfoot);
var tbody=document.createElement('tbody');

for(var i=1;i<=(r+1);i++){
	trow=document.createElement('tr');
	trow.setAttribute("id", "R"+i);
	for(var e=1;e<=(c+1);e++){
		tdata=document.createElement('td');
		tdata.setAttribute("id", "R"+i+"c"+e);
				tdata.appendChild(document.createTextNode(i+', '+e))
		trow.appendChild(tdata);
	}
	tbody.appendChild(trow);
	
};
table.appendChild(tbody);
container.appendChild(table);
};